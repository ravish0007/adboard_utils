#!/usr/bin/python3

import sys
if sys.version_info[0] < 3:
    raise Exception("Python3 required")



import argparse

parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)

parser.add_argument("-t", metavar='\b', help='banner_text')
parser.add_argument("-u", metavar='\b', help='username')
parser.add_argument("-p", metavar='\b', help='password')
args = parser.parse_args()


username = args.u
password = args.p
text     = args.t


if not all([username, password, text]):
    import sys
    print('All arguments are required')
    print('./banner_upload.py --help for more help')
    sys.exit(1)



from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument('--no-sandbox ')
driver = webdriver.Chrome('./chromedriver',options=options)


driver.get('http://52.22.88.76/login')


email = driver.find_element_by_xpath('/html/body/app-root/app-login/div/div/form/mat-card/div[1]/mat-form-field/div/div[1]/div/input')
email.send_keys(username)


password_input = driver.find_element_by_xpath('/html/body/app-root/app-login/div/div/form/mat-card/div[2]/mat-form-field/div/div[1]/div/input')
password_input.send_keys(password)


login = driver.find_element_by_xpath('/html/body/app-root/app-login/div/div/form/mat-card/div[3]/div[1]/button')
login.click()


add_advertisement = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-advertisement/div[2]/button'))
# add_advertisement = driver.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-advertisement/div[2]/button')
add_advertisement.click()
# driver.execute_script("arguments[0].click();", add_advertisement)


import datetime
now = datetime.datetime.now()

title = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-add-advertisement/div[1]/div[2]/form/div[1]/mat-form-field/div/div[1]/div/input'))
title.send_keys(now.strftime('Sample title @ %I:%M:%S %p  on %d/%b/%Y'))


categories = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id('mat-select-1'))
categories.click()
news = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath('/html/body/div[3]/div[2]/div/div/div/mat-option[3]/span'))
news.click()


date_string = now.strftime('%m/%d/%Y')
startdate = driver.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-add-advertisement/div[1]/div[2]/form/mat-form-field[1]/div/div[1]/div[1]/input')
startdate.send_keys(date_string)

now = datetime.datetime.now()
end_date = driver.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-add-advertisement/div[1]/div[2]/form/mat-form-field[2]/div/div[1]/div[1]/input')
end_date.send_keys(now.strftime('%m/%d/%Y'))

devices = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_id('mat-select-2'))
devices.click()
first_one = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath('/html/body/div[3]/div[3]/div/div/div/mat-option[1]/span'))
first_one.click()


banner = driver.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-add-advertisement/div[1]/div[2]/form/div[6]/mat-form-field/div/div[1]/div/textarea')
banner.send_keys(text)


preview = driver.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-add-advertisement/div[2]/button[2]')
preview.click()


payment = WebDriverWait(driver, 10).until(lambda x: x.find_element_by_xpath('/html/body/app-root/app-home-nav-view/div/mat-sidenav-container/mat-sidenav-content/app-preview/div[2]/button[2]/span'))
payment.click()





